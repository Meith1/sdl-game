#ifndef __AnimatedGraphic__
#define __AnimatedGraphic__

#include "ShooterObject.hpp"
#include "GameObjectFactory.hpp"

class AnimatedGraphic : public ShooterObject
{
public:

	AnimatedGraphic();
	virtual ~AnimatedGraphic() {}

	virtual void load(std::unique_ptr<LoaderParams>const& pParams);
	virtual void draw();
	virtual void update();
	virtual void clean();

private:

	int m_animSpeed;
	int m_frameCount;
};

class AnimatedGraphicCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new AnimatedGraphic();
	}
};

#endif
