#ifndef __CollisionManager__
#define __CollisionManager__

#include <vector>

class Player;
class GameObject;
class TileLayer;

class CollisionManager
{
public:

	CollisionManager();
	~CollisionManager();

	void checkPlayerEnemyBulletCollision(Player* pPlayer);
	void checkPlayerEnemyCollision(Player* pPlayer, const std::vector<GameObject*>& obejcts);
	void checkEnemyPlayerBulletCollision(const std::vector<GameObject*>& objects);
	void checkPlayerTileCollision(Player* pPlayer, const std::vector<TileLayer*>& collisionLayers);
};

#endif