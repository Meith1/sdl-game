#ifndef __Collisions__
#define __Collisions__

#include <SDL/SDL.h>

#include "Vector2D.hpp"

const static int s_buffer = 4;

static bool RectRect(SDL_Rect* A, SDL_Rect* B)
{
	int aHBuf = A->h / s_buffer;
	int aWbuf = A->w / s_buffer;

	int bHBuf = B->h / s_buffer;
	int bWbuf = B->w / s_buffer;

	// if the bottom A is less than the top of B - no collision
	if ((A->y + A->h) - aHBuf <= B->y + bHBuf) { return false; }

	// if the top of A is more than the bottom of B - no collision
	if (A->y + aHBuf >= (B->y + B->h) - bHBuf) { return false; }

	// if the right of A is less than the left of B - no collision
	if ((A->x + A->w) - aWbuf <= B->x + bWbuf) { return false; }

	// if the left of A is more than the right of B - no collision
	if (A->x + aWbuf >= (B->x + B->w) - bWbuf) { return false; }

	//othewrwise collision
	return true;
}

#endif