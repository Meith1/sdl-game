#ifndef __Enemy__
#define __Enemy__

#include "SDL/SDL.h"

#include "ShooterObject.hpp"
#include "GameObjectFactory.hpp"

class Enemy : public ShooterObject
{
public:
	
	virtual std::string type() { return "Enemy"; }

protected:

	int m_health;

	Enemy() : ShooterObject() {}
	virtual ~Enemy() {} // for polymorphism
};

#endif