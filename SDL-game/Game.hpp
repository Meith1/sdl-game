#ifndef __Game__
#define __Game__

#include "SDL/SDL.h"

#include "GameStateMachine.hpp"

#include <vector>

class Game
{
public:

	static Game* Instance()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new Game();
			return s_pInstance;
		}

		return s_pInstance;
	}

	bool init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen);
	void handleEvents();
	void update();
	void render();
	void clean();

	SDL_Renderer* getRenderer() const { return m_pRenderer; }
	SDL_Window* getWindow() const { return m_pWindow; }
	GameStateMachine* getStateMachine() { return m_pGameStateMachine; }

	void setPlayerLives(int lives) { m_playerLives = lives; }
	int getPlayerLives() { return m_playerLives; }

	void setCurrentLevel(int currentLevel);
	const int getCurrentLevel() { return m_currentLevel; }
	
	void setNextLevel(int nextLevel) { m_nextLevel = nextLevel; }
	const int getNextLevel() { return m_nextLevel; }

	bool running() { return m_bRunning; }

	void quit() { m_bRunning = false; }

	float getScrollSpeed() { return m_scrollSpeed; }

	bool changingState() { return m_bChangingState; }
	void changingState(bool cs) { m_bChangingState = cs; }

	int getGameWidth() const { return m_gameWidth; }
	int getGameHeight() const { return m_gameHeight; }

	void setLevelComplete(bool levelComplete) { m_bLevelComplete = levelComplete; }
	const bool getLevelComplete() { return m_bLevelComplete; }

	std::vector<std::string> getLevelFiles() { return m_levelFiles; }

private:

	Game();
	~Game();
	Game(const Game&);
	Game& operator=(const Game&);

	static Game* s_pInstance;

	SDL_Window* m_pWindow;
	SDL_Renderer* m_pRenderer;
	int m_gameWidth;
	int m_gameHeight;
	float m_scrollSpeed;

	int m_currentLevel;
	int m_nextLevel;
	bool m_bLevelComplete;

	int m_playerLives;

	std::vector<std::string> m_levelFiles;

	bool m_bRunning;

	GameStateMachine* m_pGameStateMachine;
	bool m_bChangingState;
};

typedef Game TheGame;

#endif