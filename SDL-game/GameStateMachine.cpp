#include "GameStateMachine.hpp"
#include "InputHandler.hpp"

void GameStateMachine::pushState(GameState* pState)
{
	m_gameStates.push_back(pState);
	m_gameStates.back()->onEnter();
}

void GameStateMachine::popState()
{
	if (!m_gameStates.empty())
	{
		if (m_gameStates.back()->onExit())
		{
			delete m_gameStates.back();
			m_gameStates.pop_back();
		}
	}
}

// new
//void GameStateMachine::changeState(GameState *pState)
//{
//	if (!m_gameStates.empty())
//	{
//		if (m_gameStates.back()->getStateID() == pState->getStateID())
//		{
//			return; // do nothing
//		}
//
//		m_gameStates.back()->onExit();
//		m_gameStates.pop_back();
//	}
//
//	// initialise it
//	pState->onEnter();
//
//	// push back our new state
//	m_gameStates.push_back(pState);
//}

// old
void GameStateMachine::changeState(GameState* pState)
{
	if (!m_gameStates.empty())
	{
		if (m_gameStates.back()->getStateID() == pState->getStateID())
		{
			return;
		}

		if (m_gameStates.back()->getIsValid())
		{
			m_gameStates.back()->setIsValid(false);
		}
	}

	m_gameStates.push_back(pState);

	m_gameStates.back()->onEnter();
}

void GameStateMachine::update()
{
	if (!m_gameStates.empty())
	{
		m_gameStates.back()->update();
	}
}

void GameStateMachine::render()
{
	if (!m_gameStates.empty())
	{
		m_gameStates.back()->render();
	}
}

void GameStateMachine::dequeState()
{
	if (!m_gameStates.empty())
	{
		if (!m_gameStates[0]->getIsValid() && m_gameStates[0]->onExit())
		{
			delete m_gameStates[0];
			m_gameStates.erase(m_gameStates.begin());
			TheInputHandler::Instance()->reset();
		}
	}
}

void GameStateMachine::clean()
{
	if (!m_gameStates.empty())
	{
		m_gameStates.back()->onExit();

		delete m_gameStates.back();

		m_gameStates.clear();
	}
}