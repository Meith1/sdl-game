#ifndef __MainMenuState__
#define __MainMenuState__

#include "MenuState.hpp"
#include "GameObject.hpp"

#include <vector>

class MainMenuState : public MenuState
{
public:

	MainMenuState();
	virtual ~MainMenuState() {}

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_menuID; }

private:

	virtual void setCallbacks(const std::vector<Callback>& callbacks);

	static const std::string s_menuID;
	std::vector<GameObject*> m_gameObjects;

	static void s_menuToPlay();
	static void s_exitFromMenu();
};

#endif