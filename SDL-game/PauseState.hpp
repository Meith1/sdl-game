#ifndef __PauseState__
#define __PauseState__

#include "MenuState.hpp"

#include <vector>

class GameObject;

class PauseState : public MenuState
{
public:

	PauseState();
	virtual ~PauseState() {}

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual void setCallbacks(const std::vector<Callback>& callbacks);

	virtual std::string getStateID() const { return s_pauseID; }

private:

	static void s_pauseToMain();
	static void s_resumePlay();

	static const std::string s_pauseID;

	std::vector<GameObject*> m_gameObjects;
};

#endif
