#ifndef __PlayState__
#define __PlayState__

#include "GameState.hpp"
#include "CollisionManager.hpp"
#include "Level.hpp"

#include <vector>

class GameObject;
class Level;

class PlayState : public GameState
{
public:

	PlayState();
	~PlayState();

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_playID; }

private:

	static const std::string s_playID;

	Level* pLevel;

	CollisionManager m_collisionManager;

	std::vector<GameObject*> m_gameObjects;
};

#endif
