#ifndef __Player__
#define __Player__

#include "SDL/SDL.h"

#include "ShooterObject.hpp"
#include "InputHandler.hpp"
#include "GameObjectFactory.hpp"

class Player : public ShooterObject
{
public:

	Player();
	virtual ~Player() {}

	virtual void load(std::unique_ptr<LoaderParams>const& pParams);
	virtual void draw();
	virtual void update();
	virtual void clean();

	virtual void collision();

	virtual std::string type() { return "Player"; }

private:

	// bring the player back if there are lives left
	void ressurect();

	// handle any input from the keyboard, mouse, or joystick
	void handleInput();

	// handle any animation for the player
	void handleAnimation();

	// player can be invincible for a time
	int m_invulnerable;
	int m_invulnerableTime;
	int m_invulnerableCounter;
};

class PlayerCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Player();
	}
};

#endif