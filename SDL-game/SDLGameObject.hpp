#ifndef __SDLGameObject__
#define __SDLGameObject__

#include "GameObject.hpp"
#include "TextureManager.hpp"
#include "Vector2D.hpp"

#include <cstring>

class SDLGameObject : public GameObject
{
public:

	SDLGameObject();
	~SDLGameObject();

	virtual void draw();
	virtual void update();
	virtual void clean();
	virtual void load(const LoaderParams* pParams);

	Vector2D& getPosition() { return m_position; }
	int getWidth() { return m_width; }
	int getHeight() { return m_height; }
	int getNumFrames();

protected:

	Vector2D m_position;
	Vector2D m_velocity;
	Vector2D m_acceleration;

	int m_width;
	int m_height;

	int m_currentRow;
	int m_currentFrame;

	std::string m_textureID;
	int m_numFrames;
};

#endif