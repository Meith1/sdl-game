#ifndef __ScrollingBackground__
#define __ScrollingBackground__

#include "ShooterObject.hpp"
#include "GameObjectFactory.hpp"

#include <SDL/SDL.h>

#include <memory>

class ScrollingBackground : public ShooterObject
{
public:
	ScrollingBackground();
	virtual ~ScrollingBackground() {}

	virtual void load(std::unique_ptr<LoaderParams>const& pParams);
	virtual void update();
	virtual void draw();
	virtual void clean();

private:

	SDL_Rect m_srcRect1;
	SDL_Rect m_srcRect2;

	SDL_Rect m_destRect1;
	SDL_Rect m_destRect2;

	int m_srcRect1Width;
	int m_srcRect2Width;

	int m_destRect1Width;
	int m_destRect2Width;

	float m_scrollSpeed;

	int count;
	int maxcount;
};

class ScrollingBackgroundCreator : public BaseCreator
{
public:

	virtual GameObject* createGameObject() const
	{
		return new ScrollingBackground();
	}
};

#endif